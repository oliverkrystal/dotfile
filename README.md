#dotfile

> well, that **should** be dotfiles

Dotfiles and other assorted goodies.  Previous to 20181124 I had a separate version of my dotfiles set up online.  I decided with Fedora 29 to rebuild the dotfile folder and reupload it to git.

Thanks to [jglovier](https://github.com/jglovier/dotfiles-logo) for an awesome logo for this repository.

>I'm like hansel and gretel, leaving breadcombs to thoughts on how to work this stuff

##Git tab-completion
I've also added tab completion to git via a contribution in the git source code.  Should be able get it doing much the same as the geany-themes because its also supposed to be a submodule and checking only the file I need via soome sparse-checkout magic.  Props to [this guy](http://clalance.blogspot.com/2011/10/git-bash-prompts-and-tab-completion.html) for figuring it out.  The commands for doing this yourself are:

	git submodule add git://github.com/git/git.git
	echo "/contrib/completion/git-completion.bash" > .git/modules/git/info/sparse-checkout
	git config --global core.sparseCheckout
	git add .gitmodules git
	git commit .gitmodules git
	git read-tree -m -u HEAD
	reload

##Submodules
To get the geany themes and the git tab-completion \(after cloning the repository you still need to do this\), you need to run `git submodule init`.  To update, run `git submodule update`.

###Geany Configurations
Geany can be found at [Geany](http://geany.org/ "Geany Project").  For linux, use your package manager. `yum install geany geany-plugins*`

I've added [geany-themes](https://github.com/codebrainz/geany-themes/) to my dotfiles as a submodule, with a symbolic link linking the files to the appropriate place.