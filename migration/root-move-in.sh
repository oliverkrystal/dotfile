#RPM Fusion
echo "Installing RPMFusion"
dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#Stuff I use all the time
echo "Installing general programs"
dnf install -y cura
dnf install -y youtube-dl
dnf install -y ffmpeg
dnf install -y chirp
dnf install -y chromium
dnf install -y wget
dnf install -y git
dnf install -y pulseaudio-equalizer
dnf install -y hexchat
dnf install -y vlc
dnf install -y geany
dnf install -y geany-plugins-*
dnf install -y gimp
dnf install -y yakuake
dnf install -y keepassxc
dnf install -y ImageMagick
dnf install -y optipng
dnf install -y php
dnf install -y grip
dnf install -y gnome-disk-utility
#VirtualBox
echo "Installing VirtualBox"
dnf install -y VirtualBox
dnf install -y system-config-users
dnf install -y akmod-VirtualBox
dnf install -y kmod-VirtualBox
#themes
echo "Installing themes"
dnf install -y breeze-icon-theme
dnf install -y rpmfusion-free-release-tainted
dnf install -y libdvdcss

#Compilation Stuff
echo "Installing Development Tools"
dnf groupinstall -y 'Development Tools'
dnf install -y automake

echo "Updating installation"
dnf -y update

echo "Root Scripts completed"
