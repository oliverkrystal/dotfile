rm .bashrc
ln -s ~/dotfile/.bashrc
chmod +x .bashrc

source ~/.bashrc

cd ~/.config && ln -s ~/dotfile/geany
cd

ln -s ~/dotfile/.gitconfig
ln -s ~/dotfile/.gitignore_global

rm -rf ~/.ssh
mkdir ~/.ssh
chmod 700 .ssh/
cd ~/.ssh
cp -r /run/media/OliverK/32CDE0503E39F77D/ssh-file/* .
chmod 600 *
chmod 644 *.pub
ln -s ~/dotfile/ssh.config config
chmod 600 config

#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/.mozilla/ .
#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/.thunderbird/ .
#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/.config/chromium/ ./config/


#cd ~/dotfile
#git submodule init
#git submodule update

cd

su -c "./root-move-in.sh"

#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/Documents/ . > /dev/null
#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/Music/ . > /dev/null
#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/Pictures/ . > /dev/null
#cp -r /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/Downloads/ . > /dev/null

#mv /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync/ /run/media/OliverK/301CBBC11CBB8100/Backups/files/$(date +"%Y%M%d")m
