#while true; do echo -ne "\e[s\e[0;$((COLUMNS-27))H$(date)\e[u"; sleep 1; done &
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

####Bash Prompt Backend Customizations, history, etc
#Sauce: http://www.numerati.com/2011/08/03/bash-goodies-turbocharging-your-history/
export HISTTIMEFORMAT="%F %T "
export HISTSIZE=10000
export HISTFILESIZE=100000
shopt -s histappend
export PROMPT_COMMAND="history -a; history -n; $PROMPT_COMMAND"
export HISTCONTROL=ignorespace

###Git "Things"
##Git status in prompt
#Sauce http://henrik.nyh.se/2008/12/git-dirty-prompt
#Sauce http://www.simplisticcomplexity.com/2008/03/13/show-your-git-branch-name-in-your-prompt/
# Must be spaced like this, herdy durr

function parse_git_deleted {
  [[ $(git status 2> /dev/null | grep deleted:) != "" ]] && echo "-"
}
function parse_git_added {
  [[ $(git status 2> /dev/null | grep "Untracked files:") != "" ]] && echo '+'
}
function parse_git_modified {
  [[ $(git status 2> /dev/null | grep modified:) != "" ]] && echo "*"
}
function parse_git_dirty {
  # [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "☠"
  echo "$(parse_git_added)$(parse_git_modified)$(parse_git_deleted)"
}
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/ $(parse_git_dirty)\1/"
}
##Git "Fix All Command" .... no, actually not.
alias yolo='git commit -am "DEAL WITH IT" && git push -f origin master'

##Show current IP Addresses in |X.X.X.X| or |X.X.X.X|X.X.X.X| etc
function show_ip () { ifconfig | grep "inet "| grep -v "127.0.0.1" |awk 'BEGIN {ORS="|"; printf "|"} { print $2}' ;}

##Add all customizations to prompt in one, here:
export PS1='[\t$(parse_git_branch)] \u@\H$(show_ip)\w α '
export PATH="$PATH:~/compiled:~/dotfile/scripts:~/binaries/:~/Builds/bam"



###Various Custom Tab Completions
##Tab completion for known ssh hosts
#Sauce: http://news.ycombinator.com/item?id=3013897
complete -W "$(echo `cat ~/.ssh/known_hosts | cut -f 1 -d ' ' | sed -e s/,.*//g | uniq | grep -v "\["`;)" ssh
#Git tab completion
source ~/dotfile/git/contrib/completion/git-completion.bash
#Nmap tab completion
#source ~/dotfile/nmap-completion/nmap


### User specific aliases and functions
##Custom Aliases
alias du="du -h"
alias df="df -h"
alias dir="ls"
alias ls="ls --color=auto -h"
alias gtkterm="su -c 'gtkterm'"
alias qmake="qmake-qt4"
alias bashrc="vi ~/.bashrc"
alias reload="source ~/.bashrc"
alias ping="ping -c 5" #because I want ping to only ping like 5 times
alias mkdir="mkdir -p" #make directories recursively
alias cp="cp -r"
alias dia="dia --integrated"
#alias vi=vim

##Because I can't type
alias sl=ls
alias la=ls

##Shortcut Aliases
alias wp5.sh='su -c "/home/OliverK/dotfile/scripts/wp5.sh"'
alias server="su -c 'firewall-cmd --add-port=8000/tcp' && python -m SimpleHTTPServer";
alias serverPHP='php -S localhost:8081'
alias monthly-backup-home='rsync -rtzuv --delete --info=progress2 /home/OliverK/ rsync://192.168.1.3:873/rsyncd/tb/Backups/files/rsync'
alias gpi-mirror='rsync -rtzuv --delete --info=progress2 /home/OliverK/Documents/RetroflagGPi/RetroPie/ rsync://192.168.1.3:873/rsyncd/tb/Backups/files/rsync/Documents/RetroflagGPi/RetroPie'
#alias gpi-mirror='rsync -rtzuv --delete --info=progress2 /home/OliverK/Documents/RetroflagGPi/RetroPie/ rsync://192.168.1.3:873/rsyncd/tb/Backups/files/rsync/Documents/RetroflagGPi/RetroPie'
alias monthly-backup-disc='rsync -rtzuv --delete --info=progress2 /run/media/OliverK/32CDE0503E39F77D/ rsync://192.168.1.3:873/rsyncd/tb/Backups/devices/usb'
#alias monthly-backup='rsync -av --delete /run/media/OliverK/32CDE0503E39F77D/ /run/media/OliverK/301CBBC11CBB8100/Backups/devices/usb && rsync -av --delete /home/OliverK/ /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync && shutdown -h now';
#alias monthly-backup='rsync -av --delete /run/media/OliverK/32CDE0503E39F77D/ /run/media/OliverK/301CBBC11CBB8100/Backups/devices/usb && rsync -av --delete /run/media/OliverK/301CBBC11CBB8100/ /run/media/OliverK/My\ Passport  && rsync -av --delete /home/OliverK/ /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync';
#alias monthly-backup='su -c "dnf update -y" && rsync -av --delete /run/media/OliverK/2064B3F45845F95D/ /run/media/OliverK/301CBBC11CBB8100/Backups/devices/usb && rsync -av --delete /run/media/OliverK/301CBBC11CBB8100/ /run/media/OliverK/My\ Passport  && rsync -av --delete /home/OliverK/ /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync && shutdown -h now';
#alias monthly-backup='su -c "dnf update -y" && rsync -av --delete /run/media/OliverK/32CDE0503E39F77D/ /run/media/OliverK/301CBBC11CBB8100/Backups/devices/usb && rsync -av --delete /run/media/OliverK/301CBBC11CBB8100/ /run/media/OliverK/My\ Passport  && rsync -av --delete /home/OliverK/ /run/media/OliverK/301CBBC11CBB8100/Backups/files/rsync && shutdown -h now';

##Various random fixes I need at one time or another
alias fix--mouse="synclient TapButton1=1 "
alias break--mouse="synclient TapButton1=0"
#Because somewhere Fedora decided that even in CLI, a gui request for my SSH password is required
export SSH_ASKPASS=""
#Build cheat for Teeworlds Server
alias build-teeworlds-server="bam server_release"

###Various Cheater Scripts, etc
##Markdown scripts
markdowntohtml ( ) { pandoc $1 -c ~/dotfile/templates/stylesheet.css --html5 --output=$1.html --to=html --from=markdown ;}
markdowntopdf ( ) { pandoc $1 -c ~/dotfile/templates/stylesheet.css --html5 --output $1.generate.html --to=html --from=markdown && wkhtmltopdf $1.generate.html $1.pdf && rm $1.generate.html ;}

##alias fix--vbox='su -c "akmods --akmod VirtualBox && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxdrv) && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxpci) && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxsf) && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxnetflt) &&  /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxnetadp) && systemctl restart systemd-modules-load.service"'
alias fix--vbox='su -c "akmods-shutdown && akmods --akmod VirtualBox && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxdrv) && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxsf) && /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxnetflt) &&  /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.priv /run/media/OliverK/32CDE0503E39F77D/Backups/driversigningkey.der $(modinfo -n vboxnetadp) && systemctl restart systemd-modules-load.service"'

##FLAAC Ugh
flac-to-mp3 () { for a in ./*.flac; do
	ffmpeg -i "$a" -qscale:a 0 "${a[@]/%flac/mp3}"
	done
}
alias dog=locate
